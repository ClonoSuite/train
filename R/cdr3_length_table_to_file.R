#' Write cdr3 length data to a file (KS-test possible).
#'
#' \code{\link{cdr3_length_grouped_bar_mean}}
#'
#' @param l A list of data.frames
#' @param ks.test.set spezifiziere die Menge aus l dessen Verteilung gegen die Restmenge (l OHNE ks.text.set) verglichen werden soll
#' @param file Name of the file
#'
#' @examples
#' # Bsp: library(RColorBrewer)
#' cbbPalette <- brewer.pal(n = 8, name = 'Set1')[-6]
#' vis_gene_usage_own_colour(lapply(l, renameToTcR2), .genes = MOUSE_TRBV, .dodge=T, .norm=T, .colours = c(as.vector(sapply(cbbPalette, function(x) rep(x,2)))))
#' @export

cdr3_length_table_to_file <- function(l, ks.test.set = NULL, file = "cdr3_length.xls") {
    if (!is.null(ks.test.set)) {
        if (!is.vector(ks.test.set)) 
            stop("ks.test.set is not a vector!")
        for (i in ks.test.set) if (i > length(l)) 
            stop(i, " in ks.test.set is out of range! length(l)=", length(l))
    }
    #---------------------------------------------------------
    # cdr3-length table
    get.matrix.cdr3.length <- function(l) {
        # length tables for tcz
        tbl.CDR3.Length <- lapply(l, function(x) {
            table(nchar(x[, 4]))
        })
        
        max.value <- max(sapply(tbl.CDR3.Length, function(x) {
            max(as.integer(names(x)))
        }))
        
        tbl.TCZ.CD3.matrix <- matrix(0, nrow = max.value, ncol = length(tbl.CDR3.Length))
        colnames(tbl.TCZ.CD3.matrix) <- names(tbl.CDR3.Length)
        
        for (n in names(tbl.CDR3.Length)) {
            tbl.TCZ.CD3.matrix[as.integer(names(tbl.CDR3.Length[[n]])), n] <- as.vector(tbl.CDR3.Length[[n]])
        }
        data.frame(tbl.TCZ.CD3.matrix)
    }
    
    # cdr3 length
    maxlength <- max(sapply(lapply(l, renameToMiTCRPattern), function(x) {
        max(nchar(x[, "CDR3.amino.acid.sequence"]))
    }))
    matrix.cdr3.length <- get.matrix.cdr3.length(lapply(l, renameToMiTCRPattern))
    matrix.cdr3.length <- data.frame(length = 1:nrow(matrix.cdr3.length), matrix.cdr3.length)
    matrix.cdr3.length_p <- data.frame(length = matrix.cdr3.length[, 1], apply(matrix.cdr3.length[, 
        -1], MARGIN = 2, function(x) {
        x/sum(x)
    }))
    
    if (!is.null(ks.test.set)) {
        m <- matrix.cdr3.length[, -1]
        min <- min(apply(m, 2, function(x) {
            min(which(x != 0))
        }))
        max <- max(apply(m, 2, function(x) {
            max(which(x != 0))
        }))
        m <- m[min:max, ]
        x <- rowMeans(m[, ks.test.set])
        y <- rowMeans(m[, setdiff(1:length(l), ks.test.set)])
        
        # norm x,y
        x <- x/sum(x)
        y <- y/sum(y)
        
        # x<-colMeans(rbind(x11,x12,x21,x22,x31,x32), na.rm=TRUE)
        # y<-colMeans(rbind(y11,y12,y21,y22,y31,y32), na.rm=TRUE)
        result <- ks.test(x, y, alternative = c("two.sided", "less", "greater"), 
            exact = NULL)
    }
    
    matrix.cdr3.length <- rbind(matrix.cdr3.length, matrix.cdr3.length_p)
    matrix.cdr3.length <- add.row.with.str.list(matrix.cdr3.length, list("", result$method, 
        paste("set x:", paste(names(l)[ks.test.set], collapse = ", ")), paste("set y:", 
            paste(names(l)[-ks.test.set], collapse = ", ")), paste("min-AA-length:", 
            min, ", max-AA-length:", max), paste("p.value =", result$p.value), paste(names(result$statistic), 
            "=", result$statistic), paste("alternative hypothesis:", result$alternative)))
    
    write.table(x = matrix.cdr3.length, file = file, sep = "\t", row.names = F)
}



add.row.with.str.list <- function(m, str.list) {
    # add new col left and shift table one to the right
    m[, ncol(m) + 1] <- ""
    m[, -1] <- m[, -ncol(m)]
    m[, 1] <- ""
    names(m) <- c("", names(m)[-ncol(m)])
    
    # add new rows for the strings
    for (str in str.list) {
        m[nrow(m) + 1, ] <- c(str, rep("", ncol(m) - 1))
    }
    return(m)
}

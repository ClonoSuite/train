## Introduction


Both B and T cells are essential for cellular immunity as key players in antigen recognition.
Next generation sequencing (NGS) technologies enable studies and analyses of the diversity of both T and B cell receptors (TCR and BCR) in human and animal systems to elucidate immune functions in health and disease.
Over the last few years, several algorithms and tools have been developed to support respective analyses of raw sequencing data of the immune repertoire.
Here we introduce TRAIN, a new R package, which offers a platform for comprehensive analyses of immune repertoires. On the one hand the package includes methods for basic statistics like gene usage and CDR3 length contribution and on the other hand providing methods for hierarchical clustering of CDR3 sequences. 
Additionally, to compare samples in respect of gene usage, amino acid sequences, TRAIN includes several functions for shared repertoire analyses.

## Installation

To download and install with all dependencies use:
```
#!link
devtools::install_bitbucket("ClonoSuite/train", dependencies=TRUE, build_vignettes = TRUE)

```

## Package Dependencies

* Project name: TRAIN
* Operating system(s): Platform independent
* Programming language: R, R Version 3.2.2 or later
* R packages data.table, dplyr, ggplot2, grid, gridExtra, gtable, igraph, Rcpp, reshape2, roxygen2, stringdist
* License: Apache v2.0 License
* Any restrictions to use by non-academics: None


## Features of TRAIN



A tab-delimited file format with following clonotype sequence information:
  
| count | frequency | CDR3nt | CDR3aa | V | D | J | Vend | Dstart | Dend |
|---|---|---|---|---|---|---|---|---|---|
| • | • | • | • | • | • | • | • | • | • |


All datasets should be converted to this format using the Convert function prior to analysis.


Formats supported for conversion:

* ClonoCalc
* miTCR
* miXCR
* immunoSEQ, one of the most commonly used RepSeq data format, more than 90\% of recently published studies were performed using immunoSEQassay.


# Statistics

The package provides functions for primary descriptive statistics for TR repertoires, including, but not
limited to:

* The distribuntion of clonotype frequnecies
* The V and J gene usage
* The distribution of CDR3 sequence lengths

For the grouped sample analysis the mean and standard deviation of samples of each group were calculated.

# Intersection, shared repertoire

Shared clonotypes and repertoire comparison:

There are several options to compare BCR or TCR repertoires, one possibility is to calculate and visualize the intersection, the shared number of clonotypes of samples, either on nucleotide or amino acid sequence.

# Sequence clustering

Hierarchical clustering of CDR3 sequences. Clustering results by calculating the distance matrices, based on CDR3 sequence. Following dissmilarity/distance measurements can be used (taken from stringdist::stringdist-metric):

* Jaccard
* Hamming
* Optimal string alignment (‘osa’) 
* Longest common substring (‘lcs’)
* Cosine
 
Further it can be distinguished whether all sequences or subsets of sequences of the same length shall be used for distance estimation.